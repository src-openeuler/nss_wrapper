%undefine __cmake_in_source_build

Name:           nss_wrapper
Version:        1.1.15
Release:        3
License:        BSD-3-Clause
Summary:        A wrapper for the user, group and hosts NSS API
Url:            https://cwrap.org/
Source0:        https://ftp.samba.org/pub/cwrap/%{name}-%{version}.tar.gz
Patch0000:      0001-Optimized-group_save-implemenation-in-case-of-selinux-attr-changes.patch
Patch0001:      nss_wrapper-fix-cmocka-1.1.6+-support.patch

BuildRequires:  gcc cmake libcmocka-devel

%description
Some projects provide daemons that need to be able to create,
modify, and delete Unix users. Or just switch user ID to interact
with the system e.g.User space file server. To be able to test whether
the user needs to modify the privileges of the passwd and groups files.
Use nss_wrapper to define your own passwd and groups files, and nss_wrapper
will use them to perform the correct operation in the test.
If you have clients and servers to test, you usually use functions to resolve
network names to addresses (dns) and vice versa. nss_wrappers allows
you to create a hosts file to set name resolution for addresses used
with socket_wrapper

To use it set the following environment variables:

LD_PRELOAD=libuid_wrapper.so
NSS_WRAPPER_PASSWD=/path/to/passwd
NSS_WRAPPER_GROUP=/path/to/group
NSS_WRAPPER_HOSTS=/path/to/host

This package doesn't have a devel package cause this project is for
development/testing.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%cmake -DUNIT_TESTING=ON
%cmake_build

%install
%cmake_install

%check
%ctest

%files
%doc AUTHORS README.md CHANGELOG
%license LICENSE
%{_bindir}/nss_wrapper.pl
%{_libdir}/libnss_wrapper.so*
%dir %{_libdir}/cmake/nss_wrapper
%{_libdir}/cmake/nss_wrapper/*.cmake
%{_libdir}/pkgconfig/nss_wrapper.pc

%files help
%{_mandir}/man1/nss_wrapper.1*

%changelog
* Fri Nov 08 2024 Funda Wang <fundawang@yeah.net> - 1.1.15-3
- adopt to new cmake macro

* Wed Jun 21 2023 yaoxin <yao_xin001@hoperun.com> - 1.1.15-2
- Fix compilation failure due to cmocka update to 1.1.7

* Mon Apr 17 2023 liyanan <thistleslyn@163.com> - 1.1.15-1
- Update to 1.1.15

* Mon May 9 2022 caodongxia <caodongxia@h-partners.com> - 1.1.11-2
- License compliance rectification

* Wed Dec 29 2021 wangkai<wangkai385@huawei.com> - 1.1.11-1
- Update to 1.1.11

* Web 02 Jun 2021 zhaoyao<zhaoyao32@huawei.com> - 1.1.3-4
- faileds: /bin/sh: gcc: command not found.

* Thu Sep 3 2020 Guoshuai Sun <sunguoshuai@huawei.com> - 1.1.3-3
- Optimized group_save implemenation in case of selinux attr changes

* Thu Nov 28 2019 yangjian<yangjian79@huawei.com> - 1.1.3-2
- Package init
